package fr.univlille.iutinfo.sql;

import java.util.List;

public record Etudiant(String nom, String prenom)
{
    public class EtudiantDAO extends DAOBasic<Etudiant>
    {
        public EtudiantDAO()
        {

        }

        public List<Etudiant> findAll()
        {
            return super.findAll();
        }
    }
}
