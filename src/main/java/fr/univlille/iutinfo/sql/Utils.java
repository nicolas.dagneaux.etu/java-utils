package fr.univlille.iutinfo.sql;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Cette classe regroupe un ensemble de requêtes SQL les plus fréquentes.
 */
public abstract class Utils
{
    /**
     * Cette méthode permet d'éxécuter une requête SELECT retournant un seul résultat (1 ligne, 1 colonne).
     * @param sqlRequest, la requête à exécuter.
     * @return Un String représentant le résultat.
     * @throws SQLException en cas d'erreur SQL.
     */
    public static String selectOneCell(String sqlRequest) throws SQLException, IOException, ClassNotFoundException {
        DBRequest<String> DBRequest = new DBRequest<String>((stmt) ->
        {
            Statement statement = stmt.createStatement();
            ResultSet resultatRequete = statement.executeQuery(sqlRequest);
            if (resultatRequete.next())
            {
                return resultatRequete.getString(1);
            }
            return null;
        });
        return DBRequest.executeRequest();
    }

    /**
     * Cette méthode permet d'éxécuter une requête SELECT retournant plusieurs colonnes, et 1 ou plusieurs lignes.
     * @param sqlRequest, la requête à exécuter.
     * @return un tableau associatif contenant le résultat de la requête, sous forme de Liste de Map.
     * Chaque entrées de la liste représente une ligne, et la clef de la Map est le nom de la colonne
     * @throws SQLException en cas d'erreur SQL.
     */
    public static List<Map<String, String>> selectMultipleKeysAsColumnsName(String sqlRequest) throws SQLException, IOException, ClassNotFoundException {

        DBRequest<List<Map<String, String>>> DBRequest = new DBRequest<List<Map<String, String>>>((stmt) ->
        {
            Statement statement = stmt.createStatement();
            ResultSet requestResult = statement.executeQuery(sqlRequest);
            //On initialise le résultat renvoyé
            ResultSetMetaData resultInfo = requestResult.getMetaData();
            //Récupération de tous les noms de colonnes
            List<String> columnList = new ArrayList<String>();
            for (int i = 0; i < resultInfo.getColumnCount(); i++)
            {
                columnList.add(resultInfo.getColumnLabel(i + 1));
            }
            List<Map<String,String>> result = new ArrayList<Map<String, String>>();
            int rowNumber = 0;
            while (requestResult.next()) {
                result.add(new HashMap<String, String>());
                for (int i = 0; i < columnList.size(); i++) {
                    result.get(rowNumber).put(columnList.get(i), requestResult.getString(i + 1));
                }
                rowNumber++;
            }
            return result;
        });
        return DBRequest.executeRequest();
    }
}
