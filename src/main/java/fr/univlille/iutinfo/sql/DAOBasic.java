package fr.univlille.iutinfo.sql;

import fr.univlille.iutinfo.sql.Pojo.Pojo;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

abstract class DAOBasic<E>
{
    private static final Map<Class, Method> sqlGetters;
    private String tableName;
    private Class<E> reflectionClass;

    public DAOBasic(Class pojoClass)
    {
        this.tableName = pojoClass.getName().toLowerCase();
    }

    static
    {
        try
        {
            sqlGetters = Map.of(
                    String.class, ResultSet.class.getDeclaredMethod("getString", int.class)
            );
        } catch (NoSuchMethodException e)
        {
            throw new RuntimeException(e);
        }
    }

    public List<E> findAll() throws SQLException, IOException, ClassNotFoundException
    {
        List<E> resultList = new ArrayList<E>();
        String sqlRequest = "SELECT * FROM " + this.tableName;
        DBLoop<List<E>> loop = new DBLoop<List<E>>(resultList, sqlRequest, (((obj, rs) ->
        {
            Constructor pojoConstructor = this.reflectionClass.getConstructors()[0];
            Object[] pojoConstructorsParam = new Object[pojoConstructor.getParameterCount()];
            for (int i = 0; i < pojoConstructor.getParameterCount(); i++)
            {
                pojoConstructorsParam[i] = DAOBasic.sqlGetters.get(this.reflectionClass.getDeclaredFields()[i]);
            }
            obj.add((E)pojoConstructor.newInstance(pojoConstructorsParam));
        })));
        return loop.executeLoop();
    }
}
