package fr.univlille.iutinfo.utils;

public interface Observer<T extends Subject, E>
{
        public void update(T subject);
        public void update(T subject, E object);
}
