package fr.univlille.iutinfo.utils;

import java.util.ArrayList;
import java.util.List;

public abstract class Subject<E>
{
    protected List<Observer> listeObserver;
    public void attach(Observer obs)
    {
        this.listeObserver.add(obs);
    }
    public void detach(Observer obs)
    {
        this.listeObserver.remove(obs);
    }
    protected void notifyObservers()
    {
        for (Observer o : listeObserver)
        {
            o.update(this);
        }
    }
    protected void notifyObservers(E data)
    {
        for (Observer o : listeObserver)
        {
            o.update(this, data);
        }
    }

    protected Subject() {
        this.listeObserver = new ArrayList<Observer>();
    }
}
