package fr.univlille.iutinfo.fxml;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

/** 
 * Cette classe abstraite définit le minimum requis pour controller une {@code Scene} JavaFX
 */
public abstract class SceneController extends Controller
{

	/**
	 * Le titre de la scène
	 */
	protected String title;
	
	/**
	 * La {@code Stage} (fenêtre) où sera affichée la scène.
	 */
	protected Stage stage;
	
	/**
	 * La {@code Scene} qui contiendra l'élément {@code Parent} root.
	 */
	protected Scene scene;
	
	/**
	 * Scene controller constructor
	 * @param fxmlPath un {@code String} représentant le chemin du fichier fxml.
	 * @param title un {@code String} représentant le titre de la scène.
	 * @param stage, l {@code Stage} (fenêtre) où sera affichée la scène.
	 */
	public SceneController(String fxmlPath, String title, Stage stage)
	{
		super(fxmlPath);
		this.title = title;
		this.stage = stage;
	}

	/**
	 * Cette méthode charge tous les éléments JavaFX et fait appel à la méthode {@code initialize}.
	 * @throws IOException en cas de problème avec le fichier .fxml.
	 */
	public final void updateStage() throws IOException
	{
		FXMLLoader loader = new FXMLLoader(new File(this.fxmlPath).toURI().toURL());
		loader.setController(this);
		this.root = loader.load();
		this.scene = new Scene(this.root);
		this.stage.setScene(this.scene);
		this.stage.setTitle(this.title);
	}

	/**
	 * @return le {@code Stage} dans lequel se situe la scène contrôlée.
	 */
	public Stage getStage()
	{
		return stage;
	}

	/**
	 * @return un {@code String} représentant le titre de la scène contrôlée
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @return la {@code Scene} contrôlée.
	 */
	public Scene getScene()
	{
		return scene;
	}
}