package fr.univlille.iutinfo.fxml;

import javafx.fxml.FXMLLoader;

import java.io.File;
import java.io.IOException;

/**
 * Cette classe abstraite définit le minimum requis pour controller une section à l'intérieur d'une fenêtre JavaFXML.
 */
public abstract class SubSectionController extends Controller
{
	
	/**
	 * @param fxmlPath un {@code String} représentant le chemin du fichier fxml.
	 */
	public SubSectionController(String fxmlPath)
	{
		super(fxmlPath);
	}

	/**
	 * Cette méthode charge tous les éléments JavaFX et fait appel à la méthode {@code initialize}.
	 * @throws IOException en cas de problème avec le fichier .fxml.
	 */
	public final void updateStage() throws IOException
	{
		FXMLLoader loader = new FXMLLoader(new File(this.fxmlPath).toURI().toURL());
		loader.setController(this);
		this.root = loader.load();
	}
}