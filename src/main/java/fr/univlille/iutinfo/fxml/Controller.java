package fr.univlille.iutinfo.fxml;

import javafx.scene.Parent;

import java.io.IOException;

/**
 * Cette classe abstraite définit le minimum requis pour controller un fichier .fxml.
 */
public abstract class Controller
{
	
	/**
	 * Un {@code String} représentant le chemin du fichier .fxml.
	 */
	protected String fxmlPath;
	
	/**
	 * Le 1er {@code Parent} du fichier .fxml.
	 */
	protected Parent root;
	
	/**
	 * Constructeur.
	 * @param fxmlPath un {@code String} représentant le chemin du fichier fxml.
	 */
	protected Controller(String fxmlPath)
	{
		this.fxmlPath = fxmlPath;
	}

	/**
	 * Cette méthode charge tous les éléments JavaFX et fait appel à la méthode {@code initialize}.
	 * @throws IOException en cas de problème avec le fichier .fxml.
	 */
	public abstract void updateStage() throws IOException;

	/**
	 * Cette méthode est appellée par le {@code FXMLLoader}.
	 * Elle est généralement utile pour initialiser les gestionnaires d'évènements.
	 */
	public abstract void initialize();

	/**
	 * Permet de récupérer l'élément parent.
	 * @return l'élément parent {@code Parent}
	 */
	public Parent getRoot()
	{
		return this.root;
	}

}