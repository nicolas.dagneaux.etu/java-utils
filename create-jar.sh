#!/usr/bin/env bash

JAVAFILES=$(find src/main/java -type f -name '*.java' | tr '\n' ' ')
PACKAGES=$(find src/main/java/fr -type d -mindepth 3 | sed 's/src\/main\/java\///' |tr '/' '.' | tr '\n' ' ')
JAVAFXFOLDER="/home/public/javafx-sdk-17.0.2"
VARGS="--module-path=$JAVAFXFOLDER:$JAVAFXFOLDER/lib --add-modules=javafx.controls,javafx.fxml"

#javadoc $VARGS -sourcepath src/main/java -d doc -docletpath lib/umldoclet-2.1.0.jar -doclet nl.talsmasoftware.umldoclet.UMLDoclet $PACKAGES

javac -g -d bin $VARGS $JAVAFILES && jar cfv univlille-iutinfo-java-utils.jar doc -C bin .
